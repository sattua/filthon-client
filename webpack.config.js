var path = require('path');

var DEV_PATH = "./src/";
var INIT_FILE = "main.jsx"; // main file...

var PRO_PATH = "./public/";
var BUILD_FILE = "bundle.js";

module.exports = {
    entry: DEV_PATH + INIT_FILE,
    output: {
        filename: BUILD_FILE,
        path: path.resolve(__dirname, PRO_PATH)
    },
    devtool: "source-map",
    module: {
        loaders: [
            {
                test: /\.js?/,
                loader: 'babel-loader?presets[]=es2015',
                query: {
                  cacheDirectory: true,
                  plugins: ['transform-decorators-legacy' ],
                  presets: ['es2015', 'react']
                }
            },
            {
                test: /\.less$/,
                loader: "style-loader!css-loader"
            }
            
        ]
    }
}