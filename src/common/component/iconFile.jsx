import React from 'react';

require('../css/iconFile.less');

export default class Iconfile extends React.Component {
    constructor(props) {
        super(props);
        this.classes = {
            'pdf':'file-pdf-o',
            'doc':'file-word-o',
            'docx':'file-word-o',
            'txt':'file-text-o',
            'png':'file-image-o',
            'jpg':'file-image-o',
            'gif':'file-image-o',
            'jpeg':'file-image-o'
        }
    }
    
    getIcon(){
        let classTemp = '';
        switch (this.props.listStyle) {
            case "list":
                classTemp = `fa fa-${this.classes[this.props.iconExtension] || 'question' }`;
            break;
            case "big":
                classTemp = `fa fa-big fa-${this.classes[this.props.iconExtension] || 'question' }`;
            break;
            case "name":
                classTemp = 'makeMeInvisible';
            break;
            default:
                classTemp = `fa fa-${this.classes[this.props.iconExtension] || 'question' }`;
            break;
        }
        return (<i className={classTemp} aria-hidden="true"></i>);
    }   
    
    getTitle(){
        let classTemp = '';
        let fileExtension = '';
        let description = 'makeMeInvisible';
        
        switch (this.props.listStyle) {
            case "list":
                fileExtension = "";
            break;
            case "name":
                fileExtension = "makeMeInvisible";
            break;
            case "big":
                classTemp = "makeMeInvisible";
            break;
            break;
            case "full":
                description = "";
            break;
            default:
                fileExtension = "makeMeInvisible";
            break;
        }
        return this.getTitleElement(classTemp, fileExtension, description);
    }
    
    getTitleElement(classTemp, fileExtension, description){
        return (<a className={classTemp} href="#viewfile" ><span className="sr-only"></span> {this.props.label || "--File without name--"} <label className={fileExtension}> {this.props.iconExtension || "--No ext--"} </label> <span className={description}>Description...</span>  </a>);
    }
    
    render() {
         return (
             <span>
                {this.getIcon()}
                {this.getTitle()}
             </span>
         );
     } 
};