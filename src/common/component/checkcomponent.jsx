import React from 'react';
import EnvValues from '../../provider/envValues.provider.jsx';

require("../css/checkcomponent.less");

//TODO: create common type map, sincro with backend
let ENVVALUES = new EnvValues();

export default class ChecksComp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visibility: false,
            selectedOptions: []
        };
        
        // This binding is necessary to make `this` work in the callback
        this.toggleState = this.toggleState.bind(this);
   }   

    toggleState(){
        if (this.state.visibility){
            this.setState({visibility: false});
        }else{
            this.setState({visibility: true });
        }
    }
    
    selectOption(event, id){
        let typeMap = ENVVALUES.fileTypesMap;
        let mappedType = typeMap[id.toLowerCase()];
        
        let list = this.state.selectedOptions;
        
        //checks if already added
        if(list.indexOf(mappedType) < 0){
            list.push(mappedType);
        }else{
            // removes duplicate ones
            list = list.filter(function(elem, index, self) {
                return elem !== mappedType;
            });
        }
        
        this.props.callSetFilterTypes( list );
        this.setState({ selectedOptions: list });
        this.toggleState();
    }

    render() {
        let message = this.props.message || "Example block-level help text here.";
        message = this.props.messageObj || 
                 (<label className="{{this.props.messageType || '' }}">This field with error</label>);
        
        var liIds = 0;     
        let typeMap = ENVVALUES.fileTypesMap;
        
        let opcions = this.props.options.map((opc) => {
            let list = this.state.selectedOptions;
            let mappedType = typeMap[opc.toLowerCase()];
            let elementClass = (list.indexOf(mappedType) >= 0) ? "checksComp-selected-opc" : "";
            
            return (<li key={'checksComp-li'+liIds++} onClick={(e) => this.selectOption(e, opc)} className={elementClass} > 
                <label >{opc}</label> 
            </li>);
        });

        return (
            <div>
                <div className="form-group">
                    <label>{this.props.label}</label>
                    <div className="dropdown">
                        <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" onClick={this.toggleState} >File Types
                        <span className="caret"></span></button>
                        <ul className={(this.state.visibility) ? 'makeMeVisible dropdown-menu' : 'dropdown-menu'}>
                            {opcions}
                        </ul>
                    </div>
                    <div className="message-block">{message}</div>
                </div>
            </div>
        );
     } 
};