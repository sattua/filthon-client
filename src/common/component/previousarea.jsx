import React from 'react';

require('../css/previousarea.less');

class Previousarea extends React.Component {
    
   render() {
       return (<div className="previousarea"><input className="previousarea-empty-input" type="text" name="empty" placeholder="No file selected..." disabled /></div>);
    } 
};

export default Previousarea;