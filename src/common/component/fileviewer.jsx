import React, {Component} from 'react';
import File from '../../model/file.jsx';
import Iconfile from './iconFile.jsx'
import Previousarea from './previousarea.jsx'

require('../css/fileviewer.less');

class Fileviewer extends React.Component {
   
   constructor(props) {
        super(props);
        this.state = {
            previouVisibility: true,
            previouLabel: "",
            fileListStyle: "list"
        };
        // This binding is necessary to make `this` work in the callback
        this.previousClickToggle = this.previousClickToggle.bind(this);
        this.setListView = this.setListView.bind(this);
        this.setBigView = this.setBigView.bind(this);
        this.setNameView = this.setNameView.bind(this);
        this.setFullView = this.setFullView.bind(this);
        this.setIconView = this.setIconView.bind(this);
   }
    
   previousVisibility(){
        if (this.state.previouVisibility){
            return (<div > <Previousarea /> </div>);
        }else{
            return (<div >  </div>);
        }
   }
   
    previousClickToggle(){
        this.previousVisibility();
       
        if (this.state.previouVisibility){
            this.setState({previouVisibility: false});
        }else{
            this.setState({previouVisibility: true });
        }
    }

    setListView(){
        this.setState({fileListStyle: "list"});
    }

    setBigView(){
        this.setState({fileListStyle: "big"});
    }
    
    setNameView(){
        this.setState({fileListStyle: "name"});
    }
    
    setFullView(){
        this.setState({fileListStyle: "full"});
    }
    
    setIconView(){
        this.setState({fileListStyle: "icon"});
    }
    
    getIconsClassNames(type){
        return  (( this.state.fileListStyle === type) ? 'iconSelected ' : '' ) 
                + "fileviewer-option-icon col-xs-2 col-sm-2 col-md-2 col-lg-2";
    }

    render() {
     let files = this.props.files || [];
     // TODO: have a unique "key" prop warning here
     var liIds = 0;
     let listItems = files.map((f) =>
         <li key={'fileviewer-li'+liIds++}><Iconfile key ={f.key} listStyle={this.state.fileListStyle} label={f.name} iconExtension={f.extension} /></li>
     );

     return (
        <div className="fileviewer"> 

            <div className="fileviewer-info-icons">
                <div className="row">
                    <div onClick={this.setNameView} className={this.getIconsClassNames('name')}>
                        <i className="fa fa-bars fa-6" aria-hidden="true"></i><label>Name only</label>
                    </div>
                    <div onClick={this.setFullView} className={this.getIconsClassNames('full')}>
                        <i className="fa fa-info fa-6" aria-hidden="true"></i><label>Full text</label>
                    </div>
                    <div onClick={this.setIconView} className={this.getIconsClassNames('icon')}>
                        <i className="fa fa-file-o fa-6" aria-hidden="true"></i><label>Icon type</label>
                    </div>
                    <div onClick={this.setBigView}  className={this.getIconsClassNames('big')}>
                        <i className="fa fa-th-large fa-6" aria-hidden="true"></i><label>Big Icon</label>
                    </div>
                    <div onClick={this.setListView}  className={this.getIconsClassNames('list')}>
                        <i className="fa fa-th-list fa-6" aria-hidden="true"></i><label>List</label>
                    </div>
                    <div onClick={this.previousClickToggle}  className={this.getIconsClassNames('')}>
                        <i className="fa fa-columns fa-6" aria-hidden="true"></i><label>Previous</label>
                    </div>
                </div>
            </div>

            <div className="fileviewer-info-btns margin-bottom">
                <div className="btn-group">
                  <a onClick={this.setNameView} className="btn btn-default" href="#">
                    <i className="fa fa-bars fa-6" title="Name only"></i>
                  </a>
                  <a onClick={this.setFullView} className="btn btn-default" href="#">
                    <i className="fa fa-info fa-6" title="Full text"></i>
                  </a>
                  <a onClick={this.setIconView} className="btn btn-default" href="#">
                    <i className="fa fa-file-o fa-6" title="Icon"></i>
                  </a>
                  <a onClick={this.setBigView} className="btn btn-default" href="#">
                    <i className="fa fa-th-large fa-6" title="Big icon"></i>
                  </a>
                  <a onClick={this.setListView} className="btn btn-default" href="#">
                    <i className="fa fa-th-list fa-6" title="List"></i>
                  </a>
                  <a onClick={this.previousClickToggle} className="btn btn-default" href="#" onClick={this.previousClickToggle} >
                    <i className="fa fa-columns fa-6" title="Previous toggle" onClick={this.previousClickToggle} ></i>
                  </a>
                </div>
            </div>
            
            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <div className="form-group">
                        <h3>Files</h3>
                        <ul className="fileviewer-listItems">
                            {listItems}
                        </ul>
                    </div>
                </div>
            </div>
            
            {this.previousVisibility()}

        </div>
    );
} 
};

export default Fileviewer;