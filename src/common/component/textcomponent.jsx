import React from 'react';

export default class Textcomp extends React.Component {
   render() {
        let message = this.props.message || "Example block-level help text here.";
        message = this.props.messageObj || 
                (<label className="{{this.props.messageType || '' }}">This field with error</label>);
       
        return (
            <div>
                
                <div className="form-group">
                    <label>{this.props.label}</label>
                    <input className="form-control" />
                    <div className="message-block">{message}</div>
                </div> 
                
            </div>
        );
    } 
};