export default class File{
    constructor(name='', extension='') {
        this.key = "file" + Date.now();
        this.name = name;
        this.extension = extension;
    }
};