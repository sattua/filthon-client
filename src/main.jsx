import React from 'react';
import ReactDOM from 'react-dom';
import {render} from 'react-dom';

import Home from './home/home.jsx';

var css = require("./common/css/main.less");

class Main extends React.Component {
   render() {
       return ( <div className="row"> 
                    <Home /> 
                </div>);
    } 
};

ReactDOM.render((
  <Main />
), document.getElementById('app'));