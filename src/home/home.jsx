import React from 'react';
import Filefinder from '../filefinder/filefinder.jsx';

require("./home.less");

class Home extends React.Component {
   render() {
        return (
                <div id="home-main" className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <Filefinder />
                </div>
        );
    } 
};

export default Home;