/*
 * Enviroment Values
 * */
export default class EnvValues {
    
    /*
     * Returns map with codes for each file type supported. It does match server ones.
     * */
    get fileTypesMap(){
        //TODO: remove harcoded values, use a web service to get them.
        return {'txt': '1', 'pdf': '2', 'jpg': '3', 'json': '4','html':'5','docx':'6','json':'7'};
    }
    
    get fileExtensions(){
        return {
                fileTypeOptions:{
                    pdf:'pdf',
                    txt:'txt',
                    html:'html',
                    docx:'docx'
                }
            }
    }
}