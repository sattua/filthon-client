import {observable, computed} from "mobx";
import File from '../model/file.jsx';

import $ from "jquery";

export default class Files {

    

    @computed get allFiles() {
		var global = this;
        return new Promise(
            function (resolve, reject) {
                $.get( "http://localhost:5000/files", function( resp ) {
                //$.get( "https://reqres.in/api/users?page=2", function( resp ) {
                    resolve(global.responseHandler(resp));
                })
                .done(function(data) {
                    resolve(data);
                })
                .fail(function(error) {
                    reject(error);
                })
                .always(function(data) {
                    resolve(data);
                });
                
        });
    }
	
    /*
     * Types - List<String>
     */
	getByType(types){
		var global = this;
		return new Promise(
            function (resolve, reject) {
                $.get( "http://localhost:5000/files?types=" + types.join(), function( resp ) {
					resolve(global.responseHandler(resp));
                })
                .done(function(data) {
                    resolve(data);
                })
                .fail(function(error) {
                    reject(error);
                })
                .always(function(data) {
                    resolve(data);
                });
                
        });
	}
	
	responseHandler(resp){
		console.log("------" + '\n' +
					"Get request method[GET]: " + '\n' +
					 "------");
		console.dir(resp);
		if(resp.results && resp.results.data.length > 0){
			let files = []
			resp.results.data.forEach(function(f) {
				files.push(new File(f.param, f.val));
			});
			return files;
		}else{
			return []; // no files in path
		}
	}
}