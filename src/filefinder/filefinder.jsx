import React from 'react';
import {observer} from 'mobx-react';

import Fileprovider from '../provider/file.provider.jsx'
import Filter from './filter/filter.jsx';

var css = require("./filefinder.less");

@observer
class Filefinder extends React.Component {
   render() {
        return (
            <div id="filefinder-main"> 
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h2>File Finder</h2>
                        <Filter observer={Fileprovider} />
                    </div>
                </div>
            </div>
        );
    }
};

export default Filefinder;