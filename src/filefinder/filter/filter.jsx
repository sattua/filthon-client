import React from 'react';
import {observer} from 'mobx-react';

import ChecksComp from '../../common/component/checkcomponent.jsx';
import Textcomp from '../../common/component/textcomponent.jsx';
import Fileviewer from '../../common/component/fileviewer.jsx';
import File from '../../model/file.jsx'


@observer
export default class Filter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            files: []
        };
		
        //TODO: move hardcoded values from filtering types.
        //Attr
        this.filter = {
                types: [1]
        }
        
		//Listeners
		this.getByType = this.getByType.bind(this);
        this.setFilterTypes = this.setFilterTypes.bind(this)
		
		//init
		this.getAllResults()
    } 

	/*
	* Calls findertemp.allFiles computed get method to get all files from server
	*/
    getAllResults(){
        if(this.props.observer){
            let findertemp = new this.props.observer(); // Observer obj
            let rawFiles = findertemp.allFiles;
            this.promiseHandler(rawFiles);
        }
    }
	
	/*
	* Calls findertemp.getByType method to get all files by extensions(file type, ex: txt) from server
	*/
	getByType(){
        if(this.props.observer){
            let findertemp = new this.props.observer();
            let rawFiles = findertemp.getByType(this.filter.types);
            this.promiseHandler(rawFiles);
        }
    }
	
	/*
    * sets this.filter.types which is search criteria for quering server.
    */
    setFilterTypes(types){
        this.filter.types = types;
    }
	
	/*
	* Will manages the promises returned by provider
	*/
	promiseHandler(rawFiles){
		let files = [];
		let global = this;
		rawFiles.then(function(response) {
			response.forEach(function (file) {
				files.push( file );
			});
			global.setState({files: files});
		},function(error) {
			//error
		});
	}
	
    render() {
        let errorTemp = (<label className="errorLabel">This field has an error</label>);    

        return (
            <div className="filter-main">      
                <Textcomp label="Search: " message="Error..." messageType="error" messageObj={errorTemp} />
                <ChecksComp callSetFilterTypes={this.setFilterTypes} label="Type: " options={['PDF','DOCX','HTML','TXT']} />
				<button onClick={this.getByType} className="btn btn-primary">Search</button>
                <Fileviewer files={this.state.files} />
            </div>
        );
    } 
};